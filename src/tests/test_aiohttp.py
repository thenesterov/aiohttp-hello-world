import pytest
import aiohttp


@pytest.mark.asyncio
async def test_hello_world():
    async with aiohttp.ClientSession() as session:
        response = await session.get('http://localhost:8080/')

        assert response.status == 200

        json_data = await response.json()

        assert json_data['code'] == 200
        assert json_data['message'] == 'Successful!!!'


if __name__ == '__main__':
    pytest.main()
