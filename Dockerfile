FROM python:3.11

EXPOSE 8080

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

RUN mkdir aiohttp-hello-world/
COPY . aiohttp-hello-world/
WORKDIR aiohttp-hello-world/

CMD python3 -m aiohttp.web -H localhost -P 8080 src.main:init
